package com.kameleoon.selenium.tests.steps.widgetsSteps.adblockPage;

import com.kameleoon.selenium.tests.pages.widgetsPage.adblockPage.AboveAdBlockPage;
import com.kameleoon.selenium.tests.utils.TimeUtils;

/**
 * Created by roman on 16.03.16.
 *
 */
public class AboveAdBlockSteps {
    private AboveAdBlockPage aboveAdBlockPage;
    public AboveAdBlockSteps() {
        aboveAdBlockPage = new AboveAdBlockPage();
    }


    public void clickStickyBlockRadio(){
        aboveAdBlockPage.getStickyBlockRadio().click();
    }

    public void saveChanges(){
        aboveAdBlockPage.getSaveButton().click();
        TimeUtils.waitForSeconds(5);
    }

}
