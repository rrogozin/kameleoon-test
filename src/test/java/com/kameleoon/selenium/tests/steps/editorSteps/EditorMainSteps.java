package com.kameleoon.selenium.tests.steps.editorSteps;

import com.kameleoon.selenium.tests.pages.editorPage.EditorMainPage;
import com.kameleoon.selenium.tests.steps.editorSteps.finalizeTestSteps.FinalizeTestSteps;
import com.kameleoon.selenium.tests.steps.editorSteps.testManagerSteps.TestManagerSteps;
import com.kameleoon.selenium.tests.utils.TimeUtils;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;

import java.util.Set;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 16.03.16.
 *
 */
public class EditorMainSteps {
    private EditorMainPage editorMainPage;
    public EditorMainSteps() {
        editorMainPage = new EditorMainPage();
    }


    public void clickVariationMenu(){
        editorMainPage.getVariationMenu().click();
    }

    public <PageObjectClass> PageObjectClass clickAddWidgetButton(Class<PageObjectClass> pageObjectClass){
        Set<String> oldWindowsSet = WebDriverFactory.getDriver().getWindowHandles();
        editorMainPage.getAddWidgetButton().click();
        TimeUtils.waitForSeconds(5);
        Set<String> newWindowsSet = WebDriverFactory.getDriver().getWindowHandles();
        newWindowsSet.removeAll(oldWindowsSet);
        String newWindowHandle = newWindowsSet.iterator().next();
        WebDriverFactory.getDriver().switchTo().window(newWindowHandle);
        WebDriverFactory.getDriver().switchTo().activeElement();

        try {
            return page(pageObjectClass.getConstructor().newInstance());
        }
        catch (Exception e) {
            throw new RuntimeException("Failed to create new instance of " + pageObjectClass, e);
        }
    }

    public String getUrl(){
        return WebDriverFactory.getDriver().getCurrentUrl();
    }

    public FinalizeTestSteps clickFinalizeTest(){
        editorMainPage.getFinalizeTestButton().click();
        TimeUtils.waitForSeconds(1);
        return page(FinalizeTestSteps.class);
    }

    public TestManagerSteps clickSelectedTestMenu(){
        editorMainPage.getSelectedTestMenu().click();
        TimeUtils.waitForSeconds(1);
        return page(TestManagerSteps.class);
    }


}
