package com.kameleoon.selenium.tests.steps.editorSteps.reconfigureTestSteps;

import com.kameleoon.selenium.tests.pages.editorPage.reconfigureTestPage.ReconfigureTestPage;
import com.kameleoon.selenium.tests.steps.editorSteps.AdvancedTargetingSteps;
import com.kameleoon.selenium.tests.utils.TimeUtils;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 28.03.16.
 *
 */
public class ReconfigureTestSteps {
    private ReconfigureTestPage reconfigureTestPage;
    public ReconfigureTestSteps() {
        reconfigureTestPage = new ReconfigureTestPage();
    }

    public AdvancedTargetingSteps chooseRunTestOnItem(String runTestOn){
        reconfigureTestPage.getRunTestOnMenu().click();
        switch (runTestOn){
            case "This page":
                reconfigureTestPage.getRunTestOnItems().get(0).click();
                break;
            case "Entire site":
                reconfigureTestPage.getRunTestOnItems().get(1).click();
                break;
            case "URLs containing a fragment":
                reconfigureTestPage.getRunTestOnItems().get(2).click();
                break;
            case "A specific advanced segment":
                reconfigureTestPage.getRunTestOnItems().get(3).click();
                break;
            case "A template segment":
                reconfigureTestPage.getRunTestOnItems().get(4).click();
                break;
        }
        TimeUtils.waitForSeconds(1);
        return page(AdvancedTargetingSteps.class);
    }


}
