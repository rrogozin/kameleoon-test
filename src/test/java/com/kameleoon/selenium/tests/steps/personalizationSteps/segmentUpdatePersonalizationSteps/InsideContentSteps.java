package com.kameleoon.selenium.tests.steps.personalizationSteps.segmentUpdatePersonalizationSteps;

import com.kameleoon.selenium.tests.pages.personalizationPage.segmentUpdatePersonalizationPage.InsideContentPage;
import com.kameleoon.selenium.tests.utils.TimeUtils;

/**
 * Created by roman on 18.03.16.
 *
 */
public class InsideContentSteps {
    private InsideContentPage insideContentPage;
    public InsideContentSteps() {
        insideContentPage = new InsideContentPage();
    }

    public void clickFromHTMLItem(){
        insideContentPage.getFromHTMLItem().click();
    }

    public void insertHtmlCode(String html){
        insideContentPage.getHtmlCodeInputText().click();
        TimeUtils.waitForSeconds(1);
        insideContentPage.getHtmlCodeInputText().sendKeys(html);
    }

    public void insertRedirectUrl(String url){
        insideContentPage.getRedirectUrlInputText().click();
        TimeUtils.waitForSeconds(1);
        insideContentPage.getRedirectUrlInputText().sendKeys(url);
    }
}
