package com.kameleoon.selenium.tests.steps.editorSteps.finalizeTestSteps;

import com.kameleoon.selenium.tests.pages.editorPage.finalizeTestPage.FinalizeTestPage;
import com.kameleoon.selenium.tests.utils.TimeUtils;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 25.03.16.
 *
 */
public class FinalizeTestSteps {
    private FinalizeTestPage finalizeTestPage;
    public FinalizeTestSteps() {
        finalizeTestPage = new FinalizeTestPage();
    }

    public RenameTestSteps clickValidateSettingsButton(){
        finalizeTestPage.getValidateSettingsButton().click();
        TimeUtils.waitForSeconds(1);
        return page(RenameTestSteps.class);
    }

    public void clickFirstVariationItem(){
        finalizeTestPage.getChooseVariationsItem().get(0).click();
    }

    public void clickChooseReportingCheckBox(){
        finalizeTestPage.getChooseReportingCheckBox().click();
    }

    public void clickChooseGoalsItem(){
        finalizeTestPage.getChooseGoalsItem().get(finalizeTestPage.getChooseGoalsItem().size() - 1).click();
    }


}
