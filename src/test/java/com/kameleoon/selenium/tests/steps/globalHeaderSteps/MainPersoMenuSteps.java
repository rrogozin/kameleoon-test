package com.kameleoon.selenium.tests.steps.globalHeaderSteps;

import com.kameleoon.selenium.tests.pages.globalHeaderPage.MainPersoMenuPage;
import com.kameleoon.selenium.tests.steps.HomePageSteps;
import com.kameleoon.selenium.tests.steps.personalizationSteps.MyPersonalizationsSteps;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 15.03.16.
 *
 */
public class MainPersoMenuSteps {
    private MainPersoMenuPage mainPersoMenuPage;
    public MainPersoMenuSteps() {
        mainPersoMenuPage = new MainPersoMenuPage();
    }

    private void moveMouseToElement(WebElement element){
        Actions actions = new Actions(WebDriverFactory.getDriver());
        actions.moveToElement(element);
        actions.perform();
    }

    public MyPersonalizationsSteps clickMyPersoMenu(){
        moveMouseToElement(mainPersoMenuPage.getMyPersoMenuElement());
        mainPersoMenuPage.getMyPersoMenuElement().click();
        return page(MyPersonalizationsSteps.class);
    }

    public HomePageSteps goToHome(){
        mainPersoMenuPage.getHomeMenuElement().click();
        return page(HomePageSteps.class);
    }
}
