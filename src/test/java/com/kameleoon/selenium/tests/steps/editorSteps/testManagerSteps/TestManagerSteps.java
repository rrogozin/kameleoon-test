package com.kameleoon.selenium.tests.steps.editorSteps.testManagerSteps;

import com.kameleoon.selenium.tests.pages.editorPage.testManagerPage.TestManagerPage;
import com.kameleoon.selenium.tests.steps.editorSteps.reconfigureTestSteps.ReconfigureTestSteps;
import com.kameleoon.selenium.tests.utils.TimeUtils;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 25.03.16.
 *

 */
public class TestManagerSteps {
    private TestManagerPage testManagerPage;
    public TestManagerSteps() {
        testManagerPage = new TestManagerPage();
    }

    private void moveMouseToElement(WebElement element){
        Actions actions = new Actions(WebDriverFactory.getDriver());
        actions.moveToElement(element);
        actions.perform();
    }

    public ReconfigureTestSteps clickConfigurationButton(){
        moveMouseToElement(testManagerPage.getRowTest().get(0));
        moveMouseToElement(testManagerPage.getConfigurationButtons().get(0));
        testManagerPage.getConfigurationButtons().get(0).click();
        TimeUtils.waitForSeconds(1);
        return page(ReconfigureTestSteps.class);
    }


}
