package com.kameleoon.selenium.tests.steps.personalizationSteps.popups;

import com.kameleoon.selenium.tests.pages.personalizationPage.popups.PopupSaveConfirmPage;
import com.kameleoon.selenium.tests.steps.personalizationSteps.UpdatePersonalizationSteps;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 22.03.16.
 *
 */
public class PopupSaveConfirmSteps {
    private PopupSaveConfirmPage popupSaveConfirmPage;
    public PopupSaveConfirmSteps() {
        popupSaveConfirmPage = new PopupSaveConfirmPage();
    }

    public UpdatePersonalizationSteps clickOk(){
        popupSaveConfirmPage.getSaveConfirmOkButton().click();
        return page(UpdatePersonalizationSteps.class);
    }

}
