package com.kameleoon.selenium.tests.steps.personalizationSteps;

import com.kameleoon.selenium.tests.pages.personalizationPage.ReportingPersonalizationPage;

/**
 * Created by roman on 18.03.16.
 *
 */
public class ReportingPersonalizationSteps {
    private ReportingPersonalizationPage reportingPersonalizationPage;
    public ReportingPersonalizationSteps() {
        reportingPersonalizationPage = new ReportingPersonalizationPage();
    }

    public void clickAddNewGoalButton(){
        reportingPersonalizationPage.getAddGoalButton().click();
    }

    public void insertGoalName(String name){
        reportingPersonalizationPage.getGoalNameInputText().sendKeys(name);
    }

    public void clickValidateButton(){
        reportingPersonalizationPage.getValidateButton().click();
    }

    public void insertGoalUrl(String url){
        reportingPersonalizationPage.getGoalUrlInputText().sendKeys(url);
    }
}
