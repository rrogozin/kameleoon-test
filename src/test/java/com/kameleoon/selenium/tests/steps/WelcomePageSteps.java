package com.kameleoon.selenium.tests.steps;

import com.kameleoon.selenium.tests.pages.WelcomePage;
import com.kameleoon.selenium.tests.steps.globalHeaderSteps.GlobalHeaderSteps;

/**
 * Created by roman on 01.03.16.
 *
 */
public class WelcomePageSteps {

    private WelcomePage welcomePage;

    public WelcomePageSteps(){
        welcomePage = new WelcomePage();
    }

    public GlobalHeaderSteps clickABtestButton(){
        welcomePage.getABtestButton().click();
        return new GlobalHeaderSteps();
    }

    public String getWelcomeBarText(){
        return welcomePage.getWelcomeBar().getText();
    }

}
