package com.kameleoon.selenium.tests.steps.personalizationSteps;

import com.kameleoon.selenium.tests.pages.personalizationPage.MyPersonalizationsPage;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 15.03.16.
 *
 */
public class MyPersonalizationsSteps {
    private MyPersonalizationsPage myPersonalizationsPage;
    public MyPersonalizationsSteps() {
        myPersonalizationsPage = new MyPersonalizationsPage();
    }


    private void moveMouseToElement(WebElement element){
        Actions actions = new Actions(WebDriverFactory.getDriver());
        actions.moveToElement(element);
        actions.perform();
    }



    public String getTileHeaderText(){
        return myPersonalizationsPage.getTitlesHeaderText().get(1).getText();
    }

    public UpdatePersonalizationSteps goToPersonalizationUpdate(){
        moveMouseToElement(myPersonalizationsPage.getTitlesHeaderText().get(1));
        myPersonalizationsPage.getEditPersoButtons().get(0).click();
        return page(UpdatePersonalizationSteps.class);
    }

    public ResultPersonalizationSteps goToPersonalizationResult(){
        moveMouseToElement(myPersonalizationsPage.getTitlesHeaderText().get(1));
        myPersonalizationsPage.getResultPersoButtons().get(0).click();
        return page(ResultPersonalizationSteps.class);
    }

    public UpdatePersonalizationSteps duplicatePersonalization(String url){
        moveMouseToElement(myPersonalizationsPage.getTitlesHeaderText().get(1));
        myPersonalizationsPage.getDuplicatePersoButtons().get(0).click();
        myPersonalizationsPage.getSelectOptionButtons().get(0).click();
        List<WebElement> list = myPersonalizationsPage.getSelectSiteItem();
        for (WebElement element : list) {
            if(element.getText().contains(url))
                element.click();
        }
        myPersonalizationsPage.getOkDuplicateButtons().get(0).click();
        return page(UpdatePersonalizationSteps.class);
    }
}
