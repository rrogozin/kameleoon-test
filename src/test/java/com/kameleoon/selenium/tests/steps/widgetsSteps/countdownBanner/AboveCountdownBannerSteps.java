package com.kameleoon.selenium.tests.steps.widgetsSteps.countdownBanner;

import com.kameleoon.selenium.tests.pages.widgetsPage.countdownBanner.AboveCountdownBannerPage;
import com.kameleoon.selenium.tests.utils.TimeUtils;

/**
 * Created by roman on 21.03.16.
 *
 */
public class AboveCountdownBannerSteps {
    private AboveCountdownBannerPage aboveCountdownBannerPage;
    public AboveCountdownBannerSteps() {
        aboveCountdownBannerPage = new AboveCountdownBannerPage();
    }

    public void clickHorizontalAlignment(){
        aboveCountdownBannerPage.getHorizontalAlignmentCheckbox().click();
    }

    public void clickVerticalAlignment(){
        aboveCountdownBannerPage.getVerticalAlignmentCheckbox().click();
    }

    public void saveWidgetAndGoToEditor(){
        aboveCountdownBannerPage.getSaveWidgetButton().click();
        TimeUtils.waitForSeconds(5);
    }

    public boolean getCheckedCheckbox(String element){
        boolean checked = false;
        switch (element) {
            case "Horizontal alignment":
                checked = aboveCountdownBannerPage.getHorizontalAlignmentChecked().isSelected();
                break;
            case "Vertical alignment":
                checked = aboveCountdownBannerPage.getVerticalAlignmentChecked().isSelected();
                break;
        }
        return checked;
    }
}
