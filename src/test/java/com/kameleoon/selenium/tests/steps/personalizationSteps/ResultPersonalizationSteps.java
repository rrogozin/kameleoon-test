package com.kameleoon.selenium.tests.steps.personalizationSteps;

import com.kameleoon.selenium.tests.pages.personalizationPage.ResultPersonalizationPage;
import com.kameleoon.selenium.tests.utils.ImageComparison;
import com.kameleoon.selenium.tests.utils.ScreenShot;

import java.io.IOException;

/**
 * Created by roman on 18.03.16.
 *
 */
public class ResultPersonalizationSteps {
    private ResultPersonalizationPage resultPersonalizationPage;
    public ResultPersonalizationSteps() {
        resultPersonalizationPage = new ResultPersonalizationPage();
    }


    public String compareScreenShotsSummarizeBlock(){
        //Original screenshot
        String imgOriginal = "./src/test/resources/screenshots/displayPersonalization" +
                "/objective_screenshot_original.jpg";
        //New screenshot
        String imgToCompareWithOriginal = "./target/screenshots/compare/displayPersonalization" +
                "/objective_screenshot_to_compare.jpg";
        //Differences screenshot
        String imgOutputDifferences = "./target/screenshots/compare/displayPersonalization" +
                "/objective_screenshot_with_changes.jpg";
        //Get new screenshot
        ScreenShot.takeScreenShotWebElement(resultPersonalizationPage.getSummarizeBlock(), imgToCompareWithOriginal);
        //Compare screenshots
        ImageComparison imageComparison = new ImageComparison(30,30,0.1);
        String result = "";
        try {
            if(imageComparison.fuzzyEqual(imgOriginal, imgToCompareWithOriginal, imgOutputDifferences)) {
                result = "Images are fuzzy-equal";
            }
            else {
                result = "Images are NOT fuzzy-equal";
                System.out.println("Look at the differences: " + imgOutputDifferences);
            }
        } catch (IOException e) { e.printStackTrace(); }
        return result;
    }


}
