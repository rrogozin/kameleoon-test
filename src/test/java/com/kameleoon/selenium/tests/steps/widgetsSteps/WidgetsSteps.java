package com.kameleoon.selenium.tests.steps.widgetsSteps;

import com.kameleoon.selenium.tests.pages.widgetsPage.WidgetsPage;
import com.kameleoon.selenium.tests.steps.widgetsSteps.adblockPage.AboveAdBlockSteps;
import com.kameleoon.selenium.tests.steps.widgetsSteps.countdownBanner.AboveCountdownBannerSteps;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 16.03.16.
 *
 */
public class WidgetsSteps {
    private WidgetsPage widgetsPage;
    public WidgetsSteps() {
        widgetsPage = new WidgetsPage();
    }

    public AboveAdBlockSteps addAdBlockInside(){
        widgetsPage.getShowAdBlock();
        widgetsPage.getAddButtons().get(widgetsPage.getAddButtons().size() - 1).click();
        widgetsPage.getAboveAdblockMenuButton().click();
        return page(AboveAdBlockSteps.class);
    }

    public AboveCountdownBannerSteps addTimerAbove(){
        widgetsPage.getAddButtons().get(0).click();
        widgetsPage.getAboveTimerMenuButton().click();
        return page(AboveCountdownBannerSteps.class);
    }
}
