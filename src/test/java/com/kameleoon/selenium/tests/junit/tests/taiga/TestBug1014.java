package com.kameleoon.selenium.tests.junit.tests.taiga;

import com.kameleoon.selenium.tests.junit.rules.DBConnectionRule;
import com.kameleoon.selenium.tests.junit.rules.ScreenShotOnFailRule;
import com.kameleoon.selenium.tests.steps.HomePageSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.configurationSteps.TestsInConfigurationSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.popups.PopupNewExperimentPageSteps;
import com.kameleoon.selenium.tests.steps.editorSteps.EditorMainSteps;
import com.kameleoon.selenium.tests.steps.editorSteps.finalizeTestSteps.FinalizeTestSteps;
import com.kameleoon.selenium.tests.steps.editorSteps.finalizeTestSteps.RenameTestSteps;
import com.kameleoon.selenium.tests.steps.editorSteps.popups.PopupFirstVisitSteps;
import com.kameleoon.selenium.tests.steps.editorSteps.popups.PopupTestSuccessStartedSteps;
import com.kameleoon.selenium.tests.steps.editorSteps.reconfigureTestSteps.ReconfigureTestSteps;
import com.kameleoon.selenium.tests.steps.editorSteps.testManagerSteps.TestManagerSteps;
import com.kameleoon.selenium.tests.steps.globalHeaderSteps.GlobalHeaderSteps;
import com.kameleoon.selenium.tests.utils.LogsManger;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.hamcrest.Matchers;
import org.junit.*;


import static com.kameleoon.selenium.tests.helpers.JavaScript.launchEditor;
import static com.kameleoon.selenium.tests.helpers.Login.login;
import static com.kameleoon.selenium.tests.helpers.Login.newLogin;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


/**
 * Created by roman on 25.03.16.
 *
 */
public class TestBug1014 {
    @ClassRule
    public static com.kameleoon.selenium.tests.junit.rules.DBConnectionRule DBConnectionRule = new DBConnectionRule();

    @Rule
    public ScreenShotOnFailRule screenShotOnFailRule = new ScreenShotOnFailRule();

    @BeforeClass
    public static void beforeTest() {
        WebDriverFactory.startBrowser(true);
    }


    @Test
    public void test(){
        newLogin();
        //login();

        /** Create new AB test */
        new HomePageSteps().clickCreateTest();
        PopupNewExperimentPageSteps popupNewExperimentPageSteps = new GlobalHeaderSteps().newTestButtonClick();
        TestsInConfigurationSteps testsInConfigurationSteps = popupNewExperimentPageSteps.createNewTest("test-CreateNewABTest");

        /** Go to Editor and.. */
        testsInConfigurationSteps.goToEditorPage();
        /** If we expect PopupFirstVisit page */
        PopupFirstVisitSteps popupFirstVisitSteps = launchEditor(PopupFirstVisitSteps.class);
        EditorMainSteps editorMainSteps = popupFirstVisitSteps.closePopup();
        /** If we expect EditorMain page */
        //EditorMainSteps editorMainSteps = launchEditor(EditorMainSteps.class);

        /** Finalize AB test */
        FinalizeTestSteps finalizeTestSteps = editorMainSteps.clickFinalizeTest();
        finalizeTestSteps.clickFirstVariationItem();
        finalizeTestSteps.clickChooseReportingCheckBox();
        finalizeTestSteps.clickChooseGoalsItem();
        RenameTestSteps renameTestSteps = finalizeTestSteps.clickValidateSettingsButton();
        PopupTestSuccessStartedSteps popupTestSuccessStartedSteps = renameTestSteps.clickStartTestButton();
        popupTestSuccessStartedSteps.clickOkButton();

        /** Configuration AB test */
        TestManagerSteps testManagerSteps = editorMainSteps.clickSelectedTestMenu();
        ReconfigureTestSteps reconfigureTestSteps = testManagerSteps.clickConfigurationButton();
        reconfigureTestSteps.chooseRunTestOnItem("A specific advanced segment");

        /** Check that the browser has no javascript errors: "Cannot read property" */
        assertThat(LogsManger.getBrowserLogException(), everyItem(Matchers.not(containsString("Cannot read property"))));
    }



    @AfterClass
    public static void afterTest() {
        WebDriverFactory.finishBrowser();
    }
}
