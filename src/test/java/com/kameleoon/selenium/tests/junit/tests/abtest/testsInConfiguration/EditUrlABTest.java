package com.kameleoon.selenium.tests.junit.tests.abtest.testsInConfiguration;

import com.kameleoon.selenium.tests.junit.rules.DBConnectionRule;
import com.kameleoon.selenium.tests.junit.rules.ScreenShotOnFailRule;
import com.kameleoon.selenium.tests.steps.HomePageSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.configurationSteps.TestsInConfigurationSteps;
import com.kameleoon.selenium.tests.steps.globalHeaderSteps.GlobalHeaderSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.popups.PopupNewExperimentPageSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.popups.PopupUpdateUrlSteps;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.junit.*;

import static com.kameleoon.selenium.tests.helpers.Login.login;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by roman on 14.03.16.
 */
public class EditUrlABTest {
    @ClassRule
    public static com.kameleoon.selenium.tests.junit.rules.DBConnectionRule DBConnectionRule = new DBConnectionRule();

    @Rule
    public ScreenShotOnFailRule screenShotOnFailRule = new ScreenShotOnFailRule();

    @BeforeClass
    public static void beforeTest() {
        WebDriverFactory.startBrowser(true);
    }


    @Test
    public void testCreateNewABTest(){
        login();
        //Create new AB test
        TestsInConfigurationSteps testsInConfigurationSteps = new HomePageSteps().clickCreateTest();
        GlobalHeaderSteps globalHeaderSteps = new GlobalHeaderSteps();
        PopupNewExperimentPageSteps popupNewExperimentPageSteps = globalHeaderSteps.newTestButtonClick();
        testsInConfigurationSteps = popupNewExperimentPageSteps.createNewTest("test-EditUrlABTest");

        //Edit url in AB test
        PopupUpdateUrlSteps popupUpdateUrlSteps = testsInConfigurationSteps.editUrl();
        testsInConfigurationSteps = popupUpdateUrlSteps.editUrl("http://www.yandex.ru");
        //Verify that url text in table is valid
        assertThat(testsInConfigurationSteps.getLinkedURLText(), equalTo("http://www.yandex.ru"));
    }

    @AfterClass
    public static void afterTest() {
        WebDriverFactory.finishBrowser();
    }
}
