package com.kameleoon.selenium.tests.junit.tests.taiga;

import com.kameleoon.selenium.tests.junit.rules.DBConnectionRule;
import com.kameleoon.selenium.tests.junit.rules.ScreenShotOnFailRule;
import com.kameleoon.selenium.tests.steps.HomePageSteps;
import com.kameleoon.selenium.tests.steps.globalHeaderSteps.MainPersoMenuSteps;
import com.kameleoon.selenium.tests.steps.personalizationSteps.ResultPersonalizationSteps;
import com.kameleoon.selenium.tests.steps.personalizationSteps.popups.PopupActivateConfirmSteps;
import com.kameleoon.selenium.tests.steps.personalizationSteps.segmentBuilderSteps.CreateNewSegmentSteps;
import com.kameleoon.selenium.tests.steps.personalizationSteps.UpdatePersonalizationSteps;
import com.kameleoon.selenium.tests.steps.personalizationSteps.MyPersonalizationsSteps;
import com.kameleoon.selenium.tests.steps.personalizationSteps.segmentBuilderSteps.segmentDefinition.PageURLSteps;
import com.kameleoon.selenium.tests.steps.personalizationSteps.segmentUpdatePersonalizationSteps.InsideContentSteps;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.junit.*;

import static com.kameleoon.selenium.tests.helpers.Login.newLogin;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by roman on 18.03.16.
 *
 */
public class TestBug914 {
    @ClassRule
    public static com.kameleoon.selenium.tests.junit.rules.DBConnectionRule DBConnectionRule = new DBConnectionRule();

    @Rule
    public ScreenShotOnFailRule screenShotOnFailRule = new ScreenShotOnFailRule();

    @BeforeClass
    public static void beforeTest() {
        WebDriverFactory.startBrowser(true);
    }


    @Test
    public void test(){
        newLogin();

        /** Create new Personalization */
        UpdatePersonalizationSteps updatePersonalizationSteps = new HomePageSteps().clickCreatePersonalizations();
        updatePersonalizationSteps.createNewPers("test-CreateNewPersTest", "test-CreateNewPersTest");

        /** Go to My Personalizations */
        MyPersonalizationsSteps myPersonalizationsSteps = new MainPersoMenuSteps().clickMyPersoMenu();

        /** Go to Update Personalizations */
        updatePersonalizationSteps = myPersonalizationsSteps.goToPersonalizationUpdate();

        /** Add a new Segment */
        CreateNewSegmentSteps createNewSegmentSteps = updatePersonalizationSteps.clickAddNewSegment();
        createNewSegmentSteps.inputNameAndDescriptionText("Segment name Test", "Segment description Test");

        /** Drag Conditions to Segment definition */
        PageURLSteps pageURLSteps = createNewSegmentSteps.dragNestableItemToSegmentsArea(PageURLSteps.class);
        pageURLSteps.inputFirstValue("http://gallery-belyaevo.ru");
        updatePersonalizationSteps = createNewSegmentSteps.clickSave();

        /** Display your personalization */
        InsideContentSteps insideContentSteps = updatePersonalizationSteps.clickInsideContentPages();
        insideContentSteps.clickFromHTMLItem();
        insideContentSteps.insertHtmlCode("Test HTML code");
        insideContentSteps.insertRedirectUrl("http://gallery-belyaevo.ru");

        /** Selection of main goal */
        updatePersonalizationSteps.chooseEngagementGoalItem();

        /** Click Activate Personalizations and return to My Personalizations */
        PopupActivateConfirmSteps popupActivateConfirmSteps = updatePersonalizationSteps.clickActivatePers();
        popupActivateConfirmSteps.clickOk();
        updatePersonalizationSteps.clickAlertClosePers();
        myPersonalizationsSteps = new MainPersoMenuSteps().clickMyPersoMenu();

        /** Go to Result Personalization */
        ResultPersonalizationSteps resultPersonalizationSteps = myPersonalizationsSteps.goToPersonalizationResult();

        /** Check that the screenshots are the same */
        assertThat(resultPersonalizationSteps.compareScreenShotsSummarizeBlock(), equalTo("Images are fuzzy-equal"));
    }

    @AfterClass
    public static void afterTest() {
        WebDriverFactory.finishBrowser();
    }
}
