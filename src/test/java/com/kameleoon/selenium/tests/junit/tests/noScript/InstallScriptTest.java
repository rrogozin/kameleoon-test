package com.kameleoon.selenium.tests.junit.tests.noScript;

import com.kameleoon.selenium.tests.configuration.properties.Properties;
import com.kameleoon.selenium.tests.junit.rules.ScreenShotOnFailRule;
import com.kameleoon.selenium.tests.steps.*;
import com.kameleoon.selenium.tests.steps.globalHeaderSteps.GlobalHeaderSteps;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.open;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by rrog on 16.01.2016.
 *
 */
public class InstallScriptTest {
    @Rule
    public ScreenShotOnFailRule screenShotOnFailRule = new ScreenShotOnFailRule();

    @Before
    public void beforeTest() {
        WebDriverFactory.startBrowser(true);
    }

    @Test
    public void testInstallScript() {
        WelcomePageSteps welcomePageSteps = open(Properties.getUrl(), LoginPageSteps.class)
                .loginAs("test", "development", WelcomePageSteps.class);
        GlobalHeaderSteps globalHeaderSteps = welcomePageSteps.clickABtestButton();
        ExperimentsPageSteps experimentsPageSteps = globalHeaderSteps.installScriptButtonClick();
        assertThat(experimentsPageSteps.getActiveButtonText(), equalTo("AB TESTING"));
    }


    @After
    public void afterTest() {
        WebDriverFactory.finishBrowser();
    }
}
