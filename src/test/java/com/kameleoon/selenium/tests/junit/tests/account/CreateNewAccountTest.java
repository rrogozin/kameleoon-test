package com.kameleoon.selenium.tests.junit.tests.account;

import com.kameleoon.selenium.tests.configuration.properties.Properties;
import com.kameleoon.selenium.tests.junit.rules.ScreenShotOnFailRule;
import com.kameleoon.selenium.tests.steps.SignupPageSteps;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.junit.*;
import com.kameleoon.selenium.tests.junit.rules.DBConnectionRule;
import com.kameleoon.selenium.tests.steps.HomePageSteps;

import static com.kameleoon.selenium.tests.helpers.Login.selectEmailInDatabase;
import static com.kameleoon.selenium.tests.helpers.Login.updatePersonalizationInDatabase;
import static com.kameleoon.selenium.tests.helpers.Login.updateScriptInDatabase;
import static com.kameleoon.selenium.tests.webtestsbase.BasePage.open;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertThat;


/**
 * Created by roman on 03.03.16.
 */
public class CreateNewAccountTest {

    @ClassRule
    public static DBConnectionRule DBConnectionRule = new DBConnectionRule();

    @Rule
    public ScreenShotOnFailRule screenShotOnFailRule = new ScreenShotOnFailRule();

    @BeforeClass
    public static void beforeTest() {
        WebDriverFactory.startBrowser(true);
    }


    @Test
    public void testCreateNewAccount(){
        //Create new account
        SignupPageSteps signupPageSteps = open(Properties.getUrl() + "/access/signup", SignupPageSteps.class);
        signupPageSteps.createNewAccount();
        //Check that a new account was created in database
        assertThat(SignupPageSteps.email, equalTo(selectEmailInDatabase()));
    }

    @Test
    public void testCreateNewAccountWithScript(){
        //Create new account
        SignupPageSteps signupPageSteps = open(Properties.getUrl() + "/access/signup", SignupPageSteps.class);
        signupPageSteps.createNewAccount();

        //Enable ActiveScript and Personalization
        updateScriptInDatabase(SignupPageSteps.email);
        updatePersonalizationInDatabase(SignupPageSteps.email);

        //Refresh page
        WebDriverFactory.getDriver().navigate().refresh();

        //Verify that module content headers is exist
        HomePageSteps homePageSteps = new HomePageSteps();

        assertThat(homePageSteps.getListContentHeaderText(),
                containsInAnyOrder("AB TESTING", "PERSONNALISATIONS", "AB TESTS MOBILE"));
    }


    @AfterClass
    public static void afterTest() {
        WebDriverFactory.finishBrowser();
    }
}
