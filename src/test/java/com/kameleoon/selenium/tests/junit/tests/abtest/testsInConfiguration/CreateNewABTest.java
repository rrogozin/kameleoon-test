package com.kameleoon.selenium.tests.junit.tests.abtest.testsInConfiguration;

import com.kameleoon.selenium.tests.steps.*;
import com.kameleoon.selenium.tests.steps.abTestSteps.configurationSteps.TestsInConfigurationSteps;

import static com.kameleoon.selenium.tests.helpers.Login.login;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import com.kameleoon.selenium.tests.steps.globalHeaderSteps.GlobalHeaderSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.popups.PopupNewExperimentPageSteps;
import org.junit.*;
import com.kameleoon.selenium.tests.junit.rules.DBConnectionRule;
import com.kameleoon.selenium.tests.junit.rules.ScreenShotOnFailRule;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;


/**
 * Created by roman on 04.03.16.
 */
public class CreateNewABTest {

    @ClassRule
    public static com.kameleoon.selenium.tests.junit.rules.DBConnectionRule DBConnectionRule = new DBConnectionRule();

    @Rule
    public ScreenShotOnFailRule screenShotOnFailRule = new ScreenShotOnFailRule();

    @BeforeClass
    public static void beforeTest() {
        WebDriverFactory.startBrowser(true);
    }


    @Test
    public void testCreateNewABTest(){
        login();
        //Create new AB test
        TestsInConfigurationSteps testsInConfigurationSteps = new HomePageSteps().clickCreateTest();
        int countRows = testsInConfigurationSteps.countRowsInTestsConfiguration();
        PopupNewExperimentPageSteps popupNewExperimentPageSteps = new GlobalHeaderSteps().newTestButtonClick();
        testsInConfigurationSteps = popupNewExperimentPageSteps.createNewTest("test-CreateNewABTest");
        //Verify that new AB Test is exist in table
        assertThat(testsInConfigurationSteps.countRowsInTestsConfiguration(), equalTo(countRows + 1));
    }

    @AfterClass
    public static void afterTest() {
        WebDriverFactory.finishBrowser();
    }

}
