package com.kameleoon.selenium.tests.junit.tests.abtest.testsInConfiguration;

import com.kameleoon.selenium.tests.junit.rules.DBConnectionRule;
import com.kameleoon.selenium.tests.junit.rules.ScreenShotOnFailRule;
import com.kameleoon.selenium.tests.steps.HomePageSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.configurationSteps.TestsInConfigurationSteps;
import com.kameleoon.selenium.tests.steps.globalHeaderSteps.GlobalHeaderSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.popups.PopupConfirmCopySteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.popups.PopupNewExperimentPageSteps;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.junit.*;

import static com.kameleoon.selenium.tests.helpers.Login.login;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by roman on 14.03.16.
 */
public class CopyABTest {
    @ClassRule
    public static com.kameleoon.selenium.tests.junit.rules.DBConnectionRule DBConnectionRule = new DBConnectionRule();

    @Rule
    public ScreenShotOnFailRule screenShotOnFailRule = new ScreenShotOnFailRule();

    @BeforeClass
    public static void beforeTest() {
        WebDriverFactory.startBrowser(true);
    }


    @Test
    public void testCopyABTest(){
        login();
        //Create new AB test
        TestsInConfigurationSteps testsInConfigurationSteps = new HomePageSteps().clickCreateTest();
        int countRows = testsInConfigurationSteps.countRowsInTestsConfiguration();
        PopupNewExperimentPageSteps popupNewExperimentPageSteps = new GlobalHeaderSteps().newTestButtonClick();
        testsInConfigurationSteps = popupNewExperimentPageSteps.createNewTest("test-CopyABTest");
        //Verify that new AB test is exist in table
        assertThat(testsInConfigurationSteps.countRowsInTestsConfiguration(), equalTo(countRows + 1));

        //Copy this AB test
        PopupConfirmCopySteps popupConfirmCopySteps = testsInConfigurationSteps.copyTest();
        popupConfirmCopySteps.clckAgree();
        //Verify that copy AB test is exist in table
        assertThat(testsInConfigurationSteps.countRowsInTestsConfiguration(), equalTo(countRows + 2));
    }

    @AfterClass
    public static void afterTest() {
        WebDriverFactory.finishBrowser();
    }
}
