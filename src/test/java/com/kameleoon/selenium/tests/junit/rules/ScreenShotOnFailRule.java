package com.kameleoon.selenium.tests.junit.rules;

import com.kameleoon.selenium.tests.utils.ScreenShot;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * Created by rrog on 16.01.2016.
 * This class represent rule - junit mechanism for adding awesome functionality in test running process.
 * This rule adds screenshot taking when test  fails
 */
public class ScreenShotOnFailRule implements TestRule {

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {

            @Override
            public void evaluate() throws Throwable {
                try {
                    base.evaluate();
                } catch (Throwable t) {
                    ScreenShot.takeScreenShot();
                    throw t;
                }
            }
        };
    }


}
