package com.kameleoon.selenium.tests.junit.tests.taiga;

import com.kameleoon.selenium.tests.junit.rules.DBConnectionRule;
import com.kameleoon.selenium.tests.junit.rules.ScreenShotOnFailRule;
import com.kameleoon.selenium.tests.steps.HomePageSteps;
import com.kameleoon.selenium.tests.steps.globalHeaderSteps.MainPersoMenuSteps;
import com.kameleoon.selenium.tests.steps.personalizationSteps.MyPersonalizationsSteps;
import com.kameleoon.selenium.tests.steps.personalizationSteps.UpdatePersonalizationSteps;
import com.kameleoon.selenium.tests.steps.personalizationSteps.popups.PopupSaveConfirmSteps;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.junit.*;

import static com.kameleoon.selenium.tests.helpers.Login.newLogin;
import static com.kameleoon.selenium.tests.helpers.settings.CreateNewSite.addNewSite;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by roman on 22.03.16.
 *
 */
public class TestBug952 {
    @ClassRule
    public static com.kameleoon.selenium.tests.junit.rules.DBConnectionRule DBConnectionRule = new DBConnectionRule();

    @Rule
    public ScreenShotOnFailRule screenShotOnFailRule = new ScreenShotOnFailRule();

    @BeforeClass
    public static void beforeTest() {
        WebDriverFactory.startBrowser(true);
    }


    @Test
    public void test(){
        newLogin();
        //login();
        addNewSite("http://belvidoor.ru");

        /** Create new Personalization */
        UpdatePersonalizationSteps updatePersonalizationSteps = new HomePageSteps().clickCreatePersonalizations();
        updatePersonalizationSteps.createNewPers("test-CreateNewPersTest", "test-CreateNewPersTest");

        /** Go to My Personalizations */
        MyPersonalizationsSteps myPersonalizationsSteps = new MainPersoMenuSteps().clickMyPersoMenu();

        /** Create new Personalization duplicate */
        myPersonalizationsSteps.duplicatePersonalization("http://belvidoor.ru");
        PopupSaveConfirmSteps popupSaveConfirmSteps = updatePersonalizationSteps.clickSavePers();
        popupSaveConfirmSteps.clickOk();

        /** Go to My Personalizations */
        myPersonalizationsSteps = new MainPersoMenuSteps().clickMyPersoMenu();

        /** Check that we have a new duplicate */
        assertThat(myPersonalizationsSteps.getTileHeaderText(), containsString("DUPLICATE"));
    }

    @AfterClass
    public static void afterTest() {
        WebDriverFactory.finishBrowser();
    }
}
