package com.kameleoon.selenium.tests.helpers.settings;

import com.kameleoon.selenium.tests.steps.globalHeaderSteps.GlobalHeaderSteps;
import com.kameleoon.selenium.tests.steps.globalHeaderSteps.MainPersoMenuSteps;
import com.kameleoon.selenium.tests.steps.settingsSteps.PreferencesSteps;
import com.kameleoon.selenium.tests.steps.settingsSteps.configurationSteps.ConfigurationSteps;

/**
 * Created by roman on 23.03.16.
 */
public class CreateNewSite {

    public static void addNewSite(String url){
        GlobalHeaderSteps globalHeaderSteps = new GlobalHeaderSteps();
        PreferencesSteps preferencesSteps = globalHeaderSteps.goToSettings();
        preferencesSteps.clickAddNewSite();
        ConfigurationSteps configurationSteps = preferencesSteps.clickConfiguration();
        configurationSteps.createNewUrl(url);
        new MainPersoMenuSteps().goToHome();
    }
}
