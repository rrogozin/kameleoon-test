package com.kameleoon.selenium.tests.pages;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.NoSuchElementException;

/**
 * Created by rrog on 16.01.2016.
 *
 */
public class WelcomePage extends BasePage {
    public WelcomePage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        try {
            return welcomeBar.isDisplayed();
        } catch (NoSuchElementException e){
            return false;
        }
    }

    @FindBy(xpath = ".//*[@class='choice_block abtest']")
    private WebElement abtestButton;

    @FindBy(xpath = ".//*[@class='choice_block personalisation']")
    private WebElement personalisationButton;

    @FindBy(xpath = ".//*[@class='welcome_bar']")
    private WebElement welcomeBar;


    public WebElement getWelcomeBar(){
        return welcomeBar;
    }

    public WebElement getABtestButton(){
        return abtestButton;
    }

    public WebElement getPersonalisationButton(){
        return personalisationButton;
    }

}
