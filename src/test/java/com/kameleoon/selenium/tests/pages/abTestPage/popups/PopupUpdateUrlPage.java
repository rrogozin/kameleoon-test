package com.kameleoon.selenium.tests.pages.abTestPage.popups;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 14.03.16.
 *
 */
public class PopupUpdateUrlPage extends BasePage {
    public PopupUpdateUrlPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return okButton.isDisplayed();
    }

    @FindBy(xpath = ".//*[contains(@id,'popup-update')]//*[contains(@class, 'green')]")
    private WebElement okButton;

    @FindBy(xpath = ".//*[contains(@id,'popup-update')]//*[contains(@class, 'cancel')]")
    private WebElement cancelButton;

    @FindBy(xpath = ".//*[contains(@id,'popup-update')]//*[contains(@class, 'area')]")
    private WebElement urlInputText;


    public WebElement getOkButton(){
        return okButton;
    }

    public WebElement getCancelButton() {
        return cancelButton;
    }

    public WebElement getUrlInputText() {
        return urlInputText;
    }
}
