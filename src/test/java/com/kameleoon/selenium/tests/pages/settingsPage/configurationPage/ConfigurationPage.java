package com.kameleoon.selenium.tests.pages.settingsPage.configurationPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 23.03.16.
 *
 */
public class ConfigurationPage extends BasePage {
    public ConfigurationPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return titleActionText.isDisplayed();
    }

    @FindBy(xpath = ".//*[contains(@class, 'sitename')]")
    private WebElement titleActionText;

    @FindBy(xpath = ".//input[contains(@class, 'input')]")
    private WebElement urlInputText;

    @FindBy(xpath = ".//*[@id='basicForm']//*[contains(@class, 'button_green')]")
    private WebElement validateButton;



    public WebElement getTitleActionText() {
        return titleActionText;
    }

    public WebElement getUrlInputText() {
        return urlInputText;
    }

    public WebElement getValidateButton() {
        return validateButton;
    }
}
