package com.kameleoon.selenium.tests.pages;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 03.03.16.
 *
 */
public class ExperimentsPage extends BasePage {
    public ExperimentsPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return titleActionText.isDisplayed();
    }

    @FindBy(xpath = ".//*[contains(@class, 'grid_5')]/h2")
    private WebElement titleActionText;

    @FindBy(xpath = ".//li[contains(@class, 'active')]//span")
    private WebElement activeButtonText;


    public WebElement getTitleActionText(){
        return titleActionText;
    }

    public WebElement getActiveButtonText(){
        return activeButtonText;
    }

}
