package com.kameleoon.selenium.tests.pages.editorPage.testManagerPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;

/**
 * Created by roman on 25.03.16.
 *
 */
public class TestManagerPage extends BasePage {
    public TestManagerPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return closeButton.isDisplayed();
    }

    @FindBy(xpath = "(.//*[@id='kameleoonQxPopIn']" +
            "//*[@class='qx-UDPopInFrame' and @qxclass='qx.ui.container.Composite']" +
            "/*[@qxclass='qx.ui.container.Composite'])[3]//*[@qxclass='qx.ui.form.Button']")
    private WebElement closeButton;

    @FindBys(@FindBy(xpath = "(.//*[@id='kameleoonQxPopIn']" +
            "//*[@class='qx-UDPopInFrame' and contains(@qxclass,'Composite')]" +
            "/*[contains(@qxclass,'Composite')])//*[contains(@qxclass,'Scroll')]" +
            "//*[contains(@class,'qx') and @qxclass='qx.ui.container.Composite']"))
    private List<WebElement> rowTest;

    @FindBys(@FindBy(xpath = ".//*[contains(@style,'action-configure')]"))
    private List<WebElement> configurationButtons;



    public List<WebElement> getConfigurationButtons() {
        return configurationButtons;
    }

    public WebElement getCloseButton() {
        return closeButton;
    }

    public List<WebElement> getRowTest() {
        return rowTest;
    }
}
