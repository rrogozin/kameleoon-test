package com.kameleoon.selenium.tests.pages.abTestPage.configurationPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;

/**
 * Created by roman on 09.03.16.
 *
 */
public class AllVariationsPage extends BasePage {
    public AllVariationsPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return titleActionText.isDisplayed();
    }

    @FindBy(xpath = ".//*[contains(@class, 'alpha')]/h2")
    private WebElement titleActionText;

    //-- Menu in Table for the first element --//
    @FindBy(xpath = ".//table//tr[2]//*[contains(@class, 'settings')]")
    private WebElement menuSettings;

    @FindBy(xpath = ".//table//tr[2]//*[contains(@class, 'delete')]")
    private WebElement deleteButton;

    @FindBys(@FindBy(xpath = ".//table//tr[2]//li[contains(@class, 'submenu')]"))
    private List<WebElement> copySubmenu;

    @FindBys(@FindBy(xpath = ".//table//tr[2]//li[contains(@class, 'copy')]"))
    private List<WebElement> copyTestButton;
    //-----------------------------------------//

    @FindBys(@FindBy(xpath = ".//table//*[contains(@class, 'table-row')]"))
    private List<WebElement> rowsTable;

    public WebElement getTitleActionText() {
        return titleActionText;
    }

    public List<WebElement> getCopySubmenu() {
        return copySubmenu;
    }

    public List<WebElement> getCopyTestButton() {
        return copyTestButton;
    }

    public WebElement getDeleteButton() {
        return deleteButton;
    }

    public WebElement getMenuSettings() {
        return menuSettings;
    }

    public List<WebElement> getRowsTable() {
        return rowsTable;
    }
}
