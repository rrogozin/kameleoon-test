package com.kameleoon.selenium.tests.pages.personalizationPage.segmentUpdatePersonalizationPage;

import com.kameleoon.selenium.tests.utils.TimeUtils;
import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 18.03.16.
 *
 */
public class InsideContentPage extends BasePage {

    public InsideContentPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return getFromHTMLItem().isDisplayed();
    }

    //-- FromHTML --//
    @FindBy(xpath = ".//*[contains(@class,'navigation-inpage-html')]")
    private WebElement fromHTMLItem;

    @FindBy(xpath = ".//*[contains(@class,'body-content-image')]//*[contains(@class,'code-html')]")
    private WebElement htmlCodeInputText;

    @FindBy(xpath = ".//*[contains(@class,'body-content-image')]//input[contains(@class,'redirect-url')]")
    private WebElement redirectUrlInputText;
    //-------------//


    private void waitForVisible(WebElement element){
        int secondsCount = 0;
        boolean isPageOpenedIndicator = element.isDisplayed();
        while (!isPageOpenedIndicator && secondsCount < 5) {
            TimeUtils.waitForSeconds(1);
            secondsCount++;
            isPageOpenedIndicator = element.isDisplayed();
        }
        if(!isPageOpenedIndicator) {
            throw new AssertionError("Page was not opened");
        }
    }


    public WebElement getFromHTMLItem() {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();"
                ,fromHTMLItem);
        return fromHTMLItem;
    }

    public WebElement getHtmlCodeInputText() {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();"
                ,htmlCodeInputText);
        waitForVisible(htmlCodeInputText);
        return htmlCodeInputText;
    }

    public WebElement getRedirectUrlInputText() {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();"
                ,redirectUrlInputText);
        waitForVisible(redirectUrlInputText);
        return redirectUrlInputText;
    }
}
