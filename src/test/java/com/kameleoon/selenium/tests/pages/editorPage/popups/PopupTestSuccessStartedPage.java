package com.kameleoon.selenium.tests.pages.editorPage.popups;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 25.03.16.
 *
 */
public class PopupTestSuccessStartedPage extends BasePage {
    public PopupTestSuccessStartedPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return okButton.isDisplayed();
    }

    @FindBy(xpath = ".//*[@id='kameleoonQxDialogs']//*[@class='qx-UDFrame' and contains(@qxclass,'editor')]" +
            "//*[contains(@class,'UDPrimaryActionButton')]")
    private WebElement okButton;


    public WebElement getOkButton() {
        return okButton;
    }
}
