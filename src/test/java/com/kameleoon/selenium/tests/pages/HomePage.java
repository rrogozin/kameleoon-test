package com.kameleoon.selenium.tests.pages;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;

/**
 * Created by roman on 04.03.16.
 *
 */
public class HomePage extends BasePage {

    public HomePage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return myContent.isDisplayed();
    }

    @FindBy(xpath = ".//*[contains(@class, 'backoffice-content')]")
    private WebElement myContent;

    @FindBys(@FindBy(xpath = ".//*[contains(@class, 'dashboard-module-header')]/h1"))
    private List<WebElement> myContentHeaderText;

    @FindBy(xpath = ".//*[contains(@class, 'alpha')]//*[contains(@class, 'action-button green')]")
    private WebElement createTestButton;

    @FindBy(xpath = ".//*[@class= 'grid_6 omega']//*[contains(@class, 'green')]")
    private WebElement createPersonalizationButton;



    public WebElement getMyContent(){
        return myContent;
    }

    public List<WebElement> getMyContentHeaderText(){
        return myContentHeaderText;
    }

    public WebElement getCreateTestButton() {
        return createTestButton;
    }

    public WebElement getCreatePersonalizationButton(){
        return createPersonalizationButton;
    }
}
