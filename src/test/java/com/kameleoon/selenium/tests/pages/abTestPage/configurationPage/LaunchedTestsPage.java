package com.kameleoon.selenium.tests.pages.abTestPage.configurationPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 09.03.16.
 *
 */
public class LaunchedTestsPage extends BasePage {
    public LaunchedTestsPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return titleActionText.isDisplayed();
    }

    @FindBy(xpath = ".//*[contains(@class, 'alpha')]/h2")
    private WebElement titleActionText;

    public WebElement getTitleActionText() {
        return titleActionText;
    }
}
