package com.kameleoon.selenium.tests.pages.abTestPage.popups;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 14.03.16.
 *
 */
public class PopupConfirmCopyPage extends BasePage {
    public PopupConfirmCopyPage() {
        super();
    }

    @FindBy(xpath = ".//*[@id='confirm-copy-popup']//*[contains(@class, 'cancel')]")
    private WebElement cancelButton;

    @FindBy(xpath = ".//*[@id='confirm-copy-popup']//*[contains(@class, 'green')]")
    private WebElement okButton;

    @Override
    public boolean isPageOpened() {
        return okButton.isDisplayed();
    }

    public WebElement getCancelButton(){
        return cancelButton;
    }

    public WebElement getOkButton() {
        return okButton;
    }
}
