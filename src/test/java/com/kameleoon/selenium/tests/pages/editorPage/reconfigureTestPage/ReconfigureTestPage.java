package com.kameleoon.selenium.tests.pages.editorPage.reconfigureTestPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;

/**
 * Created by roman on 28.03.16.
 *
 */
public class ReconfigureTestPage extends BasePage {
    public ReconfigureTestPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return updsteExperimentButton.isDisplayed();
    }

    @FindBy(xpath = "(.//*[@id='kameleoonQxPopIn']//*[@class='qx-UDPopInFrame']" +
            "/*[contains(@qxclass,'qx.ui.container.Composite')]" +
            "/*[contains(@qxclass,'qx.ui.form.Button')])[3]/*[contains(@qxclass,'qx.ui.basic.Label')]")
    private WebElement updsteExperimentButton;

    @FindBy(xpath = "((.//*[@id='kameleoonQxPopIn']//*[contains(@style,'Open Sans')]/..)" +
            "[contains(@qxclass,'Atom')]/..)[@class='qx-UDSimpleZone']")
    private WebElement runTestOnMenu;

    @FindBys(@FindBy(xpath = ".//*[@id='kameleoonQooxdooRoot']" +
            "//*[contains(@qxclass,'ListItem') and contains(@class,'UDMenuListItem')]"))
    private List<WebElement> runTestOnItems;


    public WebElement getRunTestOnMenu() {
        return runTestOnMenu;
    }

    public WebElement getUpdsteExperimentButton() {
        return updsteExperimentButton;
    }

    public List<WebElement> getRunTestOnItems() {
        return runTestOnItems;
    }
}
