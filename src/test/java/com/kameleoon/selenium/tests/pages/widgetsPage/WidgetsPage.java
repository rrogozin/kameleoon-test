package com.kameleoon.selenium.tests.pages.widgetsPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;

/**
 * Created by roman on 16.03.16.
 *
 */
public class WidgetsPage extends BasePage {
    public WidgetsPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return titleActionText.isDisplayed();
    }

    @FindBy(xpath = ".//*[contains(@class, 'page__sitename')]")
    private WebElement titleActionText;

    @FindBys(@FindBy(xpath = ".//*[contains(@class,'button_add')]"))
    private List<WebElement> addButtons;

    //--- AdBlock ---//
    @FindBy(xpath = ".//*[contains(@class,'adblock')]//*[@class='icon']")
    private WebElement showAdBlock;

    @FindBy(xpath = ".//*[@data-dropdown='adblock-inside']")
    private WebElement insideAdblockMenuButton;

    @FindBy(xpath = ".//*[@data-dropdown='adblock-above']")
    private WebElement aboveAdblockMenuButton;
    //---------------//

    //--- Countdown banner ---//
    @FindBy(xpath = ".//*[@data-dropdown='timer-inside']")
    private WebElement insideTimerMenuButton;

    @FindBy(xpath = ".//*[@data-dropdown='timer-above']")
    private WebElement aboveTimerMenuButton;
    //-----------------------//


    public WebElement getTitleActionText() {
        return titleActionText;
    }

    public WebElement getShowAdBlock() {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();",showAdBlock);
        return showAdBlock;
    }

    public List<WebElement> getAddButtons() {
        return addButtons;
    }

    public WebElement getInsideAdblockMenuButton() {
        return insideAdblockMenuButton;
    }

    public WebElement getAboveAdblockMenuButton() {
        return aboveAdblockMenuButton;
    }

    public WebElement getAboveTimerMenuButton() {
        return aboveTimerMenuButton;
    }

    public WebElement getInsideTimerMenuButton() {
        return insideTimerMenuButton;
    }
}
