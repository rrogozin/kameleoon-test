package com.kameleoon.selenium.tests.pages.settingsPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;

/**
 * Created by roman on 23.03.16.
 *
 */
public class PreferencesPage extends BasePage {
    public PreferencesPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return titleActionText.isDisplayed();
    }

    @FindBy(xpath = ".//*[contains(@class, 'sitename')]")
    private WebElement titleActionText;

    @FindBy(xpath = ".//*[contains(@class, 'new-site-button')]")
    private WebElement newSiteButton;

    @FindBys(@FindBy(xpath = ".//*[contains(@class, 'general-link')]"))
    private List<WebElement> configurationButtons;


    public WebElement getTitleActionText() {
        return titleActionText;
    }

    public WebElement getNewSiteButton() {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();"
                ,newSiteButton);
        return newSiteButton;
    }

    public List<WebElement> getConfigurationButtons() {
        return configurationButtons;
    }
}
