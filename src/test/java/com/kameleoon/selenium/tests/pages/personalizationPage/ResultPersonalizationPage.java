package com.kameleoon.selenium.tests.pages.personalizationPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 18.03.16.
 *
 */
public class ResultPersonalizationPage extends BasePage {
    public ResultPersonalizationPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return persoStatusMenu.isDisplayed();
    }

    @FindBy(xpath = ".//*[@class='perso_status']//*[contains(@class, 'select_option_btn')]")
    private WebElement persoStatusMenu;

    @FindBy(xpath = ".//*[contains(@id,'summarize_block')]")
    private WebElement summarizeBlock;

    public WebElement getSummarizeBlock() {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();"
                , summarizeBlock);
        return summarizeBlock;
    }
}
