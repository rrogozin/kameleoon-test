package com.kameleoon.selenium.tests.pages.personalizationPage.popups;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 14.03.16.
 *
 */
public class PopupActivateConfirmPage extends BasePage {
    public PopupActivateConfirmPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return activateConfirmOkButton.isDisplayed();
    }

    @FindBy(xpath = ".//*[@id='activate_confirm']//*[contains(@class,'green')]")
    private WebElement activateConfirmOkButton;


    public WebElement getActivateConfirmOkButton() {
        return activateConfirmOkButton;
    }
}
