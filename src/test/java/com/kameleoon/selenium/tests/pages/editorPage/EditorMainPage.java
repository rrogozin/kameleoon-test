package com.kameleoon.selenium.tests.pages.editorPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 16.03.16.
 *
 */
public class EditorMainPage extends BasePage {
    public EditorMainPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return variationMenu.isDisplayed();
    }

    @FindBy(xpath = ".//*[@id='kameleoonQxTopPanel']//*[contains(@class,'VariationTabSelected')]" +
            "//*[contains(@qxclass,'MenuButton')]//*[contains(@qxclass,'Image')]")
    private WebElement variationMenu;

    @FindBy(xpath = ".//*[@id='kameleoonQooxdooRoot']/div[@class='qx-UDMenuOpen']/div[2]/div[1]")
    private WebElement addWidgetButton;

    @FindBy(xpath = ".//*[@id='kameleoonQxTopPanel']//*[contains(@class,'ActionButton')]")
    private WebElement finalizeTestButton;

    @FindBy(xpath = ".//*[@id='kameleoonQxTopPanel']//*[@class='qx-UDSimpleZone']")
    private WebElement selectedTestMenu;

    public WebElement getVariationMenu() {
        return variationMenu;
    }

    public WebElement getAddWidgetButton() {
        return addWidgetButton;
    }

    public WebElement getFinalizeTestButton() {
        return finalizeTestButton;
    }

    public WebElement getSelectedTestMenu() {
        return selectedTestMenu;
    }
}
