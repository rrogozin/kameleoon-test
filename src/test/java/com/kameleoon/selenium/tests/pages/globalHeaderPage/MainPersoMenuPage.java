package com.kameleoon.selenium.tests.pages.globalHeaderPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 15.03.16.
 *
 */
public class MainPersoMenuPage extends BasePage {

    public MainPersoMenuPage(){
        super();
    }

    @Override
    public boolean isPageOpened() {
        return getHomeMenuElement().isDisplayed();
    }

    @FindBy(xpath = ".//*[contains(@class, 'home menu')]")
    private WebElement homeMenuElement;

    @FindBy(xpath = ".//*[contains(@class, 'personalization menu')]")
    private WebElement myPersoMenuElement;

    public WebElement getHomeMenuElement() {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();"
                ,homeMenuElement);
        return homeMenuElement;
    }

    public WebElement getMyPersoMenuElement() {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();"
                ,myPersoMenuElement);
        return myPersoMenuElement;
    }
}
