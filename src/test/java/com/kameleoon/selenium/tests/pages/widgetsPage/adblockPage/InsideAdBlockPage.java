package com.kameleoon.selenium.tests.pages.widgetsPage.adblockPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;

/**
 * Created by roman on 16.03.16.
 *
 */
public class InsideAdBlockPage extends BasePage {
    public InsideAdBlockPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return false;
    }
}
