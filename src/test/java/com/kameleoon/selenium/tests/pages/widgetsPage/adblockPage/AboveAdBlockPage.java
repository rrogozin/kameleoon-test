package com.kameleoon.selenium.tests.pages.widgetsPage.adblockPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 16.03.16.
 *
 */
public class AboveAdBlockPage extends BasePage {
    public AboveAdBlockPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return getBackToWidgetsButton().isDisplayed();
    }

    @FindBy(xpath = ".//*[contains(@class,'adblock-above')]//*[@class='back_position']")
    private WebElement backToWidgetsButton;

    //-- Radio --//
    @FindBy(xpath = ".//*[@id='overpage-plugin-adblock-radio5-value2']")
    private WebElement stickyBlockRadio;
    //----------//

    @FindBy(xpath = ".//*[contains(@class,'save')]")
    private WebElement saveButton;

    public WebElement getBackToWidgetsButton() {
        ((JavascriptExecutor) WebDriverFactory.getDriver())
                .executeScript("arguments[0].scrollIntoView();",backToWidgetsButton);
        return backToWidgetsButton;
    }

    public WebElement getStickyBlockRadio() {
        return stickyBlockRadio;
    }

    public WebElement getSaveButton() {
        ((JavascriptExecutor) WebDriverFactory.getDriver())
                .executeScript("arguments[0].scrollIntoView();",saveButton);
        return saveButton;
    }
}
