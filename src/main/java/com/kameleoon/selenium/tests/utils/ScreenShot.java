package com.kameleoon.selenium.tests.utils;

import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.internal.Locatable;


import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by roman on 02.03.16.
 */
public class ScreenShot {

    private static String DIRECTORY = "./target/screenshots/";

    /**
     * This method calls in tests listeners on test fail
     */
    public static void takeScreenShot() {
        try {
            File scrFile = ((TakesScreenshot) WebDriverFactory.getDriver()).getScreenshotAs(OutputType.FILE);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");
            File newFileName = new File(String.format(DIRECTORY + "ScreenShot " + dateFormat.format(new Date()) + ".png"));
            scrFile.renameTo(newFileName);
            System.out.println("New screenShot files created: " + String.format(newFileName.getAbsolutePath()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void takeScreenShotWebElement(WebElement element , String directory) {
        //((JavascriptExecutor)WebDriverFactory.getDriver()).executeScript("arguments[0].style.border='3px solid red'", element);
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();", element);
        File screenshot = ((TakesScreenshot)WebDriverFactory.getDriver()).getScreenshotAs(OutputType.FILE);
        BufferedImage img = null;
        try {
            img = ImageIO.read(screenshot);
        } catch (IOException e) {
            e.printStackTrace();
        }

        int height = element.getSize().getHeight();
        int width = element.getSize().getWidth();

        int pX = element.getLocation().getX() + 350;
        int pY = element.getLocation().getY() + 300;
        BufferedImage dest = img.getSubimage(pX, pY, width + 1100, height + 500);

        try {
            ImageIO.write(dest, "JPEG", screenshot);
            //FileUtils.copyFile(screenshot, new File(String.format(DIRECTORY + "ScreenShot_WebElement " + ".png")));
            FileUtils.copyFile(screenshot, new File(String.format(directory)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




}
