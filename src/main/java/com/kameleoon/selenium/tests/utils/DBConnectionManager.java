package com.kameleoon.selenium.tests.utils;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import com.kameleoon.selenium.tests.configuration.properties.Properties;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Created by roman on 03.03.16.
 */
public class DBConnectionManager {

    // JDBC URL, username and password of MySQL server
//    private static final String url = "jdbc:mysql://localhost:3306/net_kameleoon-dev_kameleoon";
//    private static final String user = "kameleoon-dev";
//    private static final String password = "kameleoon_dev";
    private static final String fullUrl = Properties.getDbConnection();

    // JDBC variables for opening and managing connection
    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;
    private static PreparedStatement preparedStmt;

    public void open(){
        try {
            // opening database connection to MySQL server
//            con = (Connection) DriverManager.getConnection(url, user, password);
            con = (Connection) DriverManager.getConnection(fullUrl);
        }
        catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    public void close(){
        //close connection ,stmt and resultset here
        try {
            if (rs != null) { rs.close(); }
            if (stmt != null) { stmt.close(); }
            if (con != null) { con.close(); }
        } catch (SQLException se) { se.printStackTrace(); }
    }

    public String execute(String query){
        String response = null;
        try {
            // getting Statement object to execute query
            stmt = (Statement) con.createStatement();
            // executing SELECT query
            rs = stmt.executeQuery(query);
            while (rs.next()) { response = rs.getString(1); }
        }
        catch (SQLException sqlEx) { sqlEx.printStackTrace(); }
        return response;
    }

    public void executeUpdate(String query){
        String response = null;
        try {
            // create the java mysql update preparedstatement
            preparedStmt = (PreparedStatement) con.prepareStatement(query);
            // execute the java preparedstatement
            preparedStmt.executeUpdate();
        }
        catch (SQLException sqlEx) { sqlEx.printStackTrace(); }
        try { preparedStmt.close(); } catch(SQLException se) { se.printStackTrace(); }
    }


    //----------------------------------------------------------------

    public static void main(String[] args) {
        TestConnection();
    }

    private static void TestConnection() {
        String query = "SELECT email FROM account WHERE email LIKE '%@test%' ORDER BY id DESC LIMIT 1";

        try {
            // opening database connection to MySQL server
            con = (Connection) DriverManager.getConnection(fullUrl);

            // getting Statement object to execute query
            stmt = (Statement) con.createStatement();

            // executing SELECT query
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                String testS = rs.getString(1);
                int testI = rs.getRow();
                System.out.println("testS: " + testS);
                //System.out.println("testI: " + testI);
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            //close connection ,stmt and resultset here
            try { con.close(); } catch(SQLException se) { /*can't do anything */ }
            try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
            try { rs.close(); } catch(SQLException se) { /*can't do anything */ }
        }
    }

}
