package com.kameleoon.selenium.tests.webtestsbase;

import com.kameleoon.selenium.tests.utils.TimeUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by rrog on 16.01.2016.
 * This is the main class for pages. When you create your page - you must extend your class from this
 */
public abstract class BasePage {
    protected static final int WAIT_FOR_PAGE_LOAD_IN_SECONDS = 5;
    /**
     * checks is page opened
     * @return true if opened
     */
    public abstract boolean isPageOpened();

    public BasePage(){
        PageFactory.initElements(getDriver(), this);
        waitForOpen();
    }

    /**
     * Waiting for page opening
     */
    private void waitForOpen(){
        int secondsCount = 0;
        boolean isPageOpenedIndicator = isPageOpened();
        while (!isPageOpenedIndicator && secondsCount < WAIT_FOR_PAGE_LOAD_IN_SECONDS) {
            TimeUtils.waitForSeconds(1);
            secondsCount++;
            isPageOpenedIndicator = isPageOpened();
        }
        if(!isPageOpenedIndicator) {
            throw new AssertionError("Page was not opened");
        }
    }

    /**
     * getting webdriver instance
     * @return initialized in tests webdriver instance
     */
    public static WebDriver getDriver(){
        return WebDriverFactory.getDriver();
    }

    //---------------------------------------------------------

    public static void open(String url) {
        WebDriver webdriver = getDriver();
        webdriver.navigate().to(url);
    }

    public static <PageObjectClass> PageObjectClass open(String relativeOrAbsoluteUrl,
                                                         Class<PageObjectClass> pageObjectClassClass) {
        open(relativeOrAbsoluteUrl);
        return page(pageObjectClassClass);
    }

    /**
     * Create a Page Object instance.
     * @see PageFactory#initElements(WebDriver, Class)
     */
    public static <PageObjectClass> PageObjectClass page(Class<PageObjectClass> pageObjectClass) {
        try {
            return page(pageObjectClass.getConstructor().newInstance());
        } catch (Exception e) {
            throw new RuntimeException("Failed to create new instance of " + pageObjectClass, e);
        }
    }

    public static <PageObjectClass, T extends PageObjectClass> PageObjectClass page(T pageObject) {
        PageFactory.initElements(getDriver(), pageObject);
        return pageObject;
    }

}
